package execute

import "testing"

func TestCommand(t *testing.T) {
	type args struct {
		cmd string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "run a command that exists",
			args: args{
				cmd: "echo Hello World",
			},
			wantErr: false,
		},
		{
			name: "run a command that doesn't exist",
			args: args{
				cmd: "slartibartfast Hello World",
			},
			wantErr: true,
		},
		{
			name: "run a command that fails",
			args: args{
				cmd: "false",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Command(tt.args.cmd); (err != nil) != tt.wantErr {
				t.Errorf("Command() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
