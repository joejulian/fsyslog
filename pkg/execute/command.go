package execute

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// Command splits a string into command and args, and executes it
func Command(cmd string) error {
	parts := strings.Fields(cmd)
	head := parts[0]
	parts = parts[1:]
	c := exec.Command(head, parts...)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr

	if err := c.Run(); err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}
