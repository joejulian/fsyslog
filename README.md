# fsyslog

fsyslog is a fake syslog processor that simply listens to the log socket and prints the output to stdout.

The intended use case is for container images for legacy applications that only write to syslog (eg. postfix).

## Usage

```bash
fsyslog -command "postfix start-fg"
```

```syslog
Listening to /dev/log...
Executing postfix start-fg
/usr/lib/postfix/bin/postfix-script: line 331: egrep: command not found
postfix/postfix-script: warning: not owned by postfix: .
mail.warning:Mar  4 06:42:15 postfix/postfix-script[224]: warning: not owned by postfix: .
postfix/postfix-script: warning: not owned by postfix: ./pid
mail.warning:Mar  4 06:42:15 postfix/postfix-script[225]: warning: not owned by postfix: ./pid
postfix/postfix-script: starting the Postfix mail system
mail.info:Mar  4 06:42:15 postfix/postfix-script[230]: starting the Postfix mail system
mail.info:Mar  4 06:42:15 postfix/master[231]: daemon started -- version 3.4.0, configuration /etc/postfix
```

## Building fsyslog

```bash
go get -d gitlab.com/joejulian/fsyslog
cd $GOPATH/src/gitlab.com/joejulian/fsyslog
go build -o fsyslog
```