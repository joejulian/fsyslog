module gitlab.com/joejulian/fsyslog

go 1.12

require (
	github.com/joejulian/go-init v0.0.0-20190503004839-9c55ee889a4a
	github.com/kr/pretty v0.2.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/mcuadros/go-syslog.v2 v2.3.0
)
