package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gopkg.in/mcuadros/go-syslog.v2"

	"github.com/joejulian/go-init/pkg/sysinit"
)

var path string
var command string

func main() {
	var wg sync.WaitGroup

	flag.StringVar(&path, "path", "/dev/log", "path to use a syslog socket")
	flag.StringVar(&command, "command", "logger log something", "command to execute using this logger")
	flag.Parse()

	// remove stale syslog socket
	if err := os.Remove(path); err != nil {
		log.Println(err)
	}

	// Prepare a backgrond context that can be canceled
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Routine to listen for signals and cancel if received
	go func() {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

		go func() {
			<-sigs
			cancel()
		}()
		<-ctx.Done()
	}()

	// Routine to reap zombies (it's the job of init)
	wg.Add(1)
	go sysinit.RemoveZombies(ctx, &wg)

	// Routine to listen and print syslog entries
	fmt.Printf("Listening to %s...\n", path)
	wg.Add(1)
	go func() {
		defer wg.Done()
		channel := make(syslog.LogPartsChannel)
		handler := syslog.NewChannelHandler(channel)

		server := syslog.NewServer()
		server.SetFormat(syslog.RFC3164)
		server.SetHandler(handler)
		if err := server.ListenUnixgram(path); err != nil {
			log.Panic(err)
		}
		if err := server.Boot(); err != nil {
			log.Panic(err)
		}

		go func(channel syslog.LogPartsChannel) {
			for logParts := range channel {
				j, err := json.Marshal(logParts)
				if err != nil {
					log.Panic(err)
				}
				fmt.Println(string(j))
			}
		}(channel)

		server.Wait()
	}()

	log.Printf("Executing %s\n", command)
	err := sysinit.Run(command)
	if err != nil {
		log.Println(err)
		sysinit.CleanQuit(cancel, &wg, 1)
	}

	sysinit.CleanQuit(cancel, &wg, 0)
}
